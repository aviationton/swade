## 0.0.1 (2020-05-25)


### Features

* initial release
* translated sheet

### 0.0.2 e 0.0.3 (2020-07-20)

* update according to the new swade module version
* minor fixes
