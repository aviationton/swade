/* global Hooks, Babele */

Hooks.on('init', () => {
  if (typeof Babele !== 'undefined') {
    Babele.get().register({
      module: 'swade_pt-BR',
      lang: 'pt-BR',
      dir: 'lang/pt-BR/compendium'
    });
  }
});
