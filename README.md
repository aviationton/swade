# FoundryVTT SWADE Brazilian Portuguese

## Português 
Esse módulo adiciona o idioma Português (Brasil) como uma opção a ser selecionada nas configurações do FoundryVTT. 
Selecionar essa opção traduzirá a ficha de personagem do sistema SWADE e mais alguns aspectos menores. 
No momento, os compêndios não estão traduzidos.
Esse módulo traduz somente aspectos relacionados ao sistema de [SWADE](https://gitlab.com/florad-foundry/swade/). 
Esse módulo não traduz outras partes do software FoundryVTT, como a interface principal. 
Para isso, confira o módulo [Brazillian Portuguese Core](https://foundryvtt.com/packages/ptBR-core/).

Se encontrar algum erro na tradução ou tiver uma sugestão de algum termo melhor para ser usado em alguma parte dela você abra uma nova issue.

### Instalação
A tradução está disponível na lista de Módulos Complementares para instalar com o nome de Translation: Brazilian Portuguese [SWADE].

### Instalação por Manifesto
Na opção Add-On Modules clique em Install Module e coloque o seguinte link no campo Manifest URL

`https://gitlab.com/foundryvtt-pt-br/swade/-/raw/master/swade_pt-BR/module.json`

### Instalação Manual
Se as opções acima não funcionarem, faça o download do arquivo [swade-master-swade_pt-BR.zip](https://gitlab.com/foundryvtt-pt-br/swade/-/archive/master/swade-master.zip?path=swade_pt-BR) e extraia a pasta swade_pt-BR dentro da pasta Data/modules/
Feito isso ative o módulo nas configurações do mundo em que pretende usá-lo e depois altere o idioma nas configurações.


## English
This module adds the Portuguese (Brazil) language as an option to be selected in the FoundryVTT settings. 
Selecting this option will translate the character sheet of the SWADE system and some minor aspects. 
At the moment, the textbooks are not translated.
This module only translates aspects related to the [SWADE] system (https://gitlab.com/florad-foundry/swade/). 
This module does not translate other parts of the FoundryVTT software, such as the main interface. 
For this, check out the module [Brazillian Portuguese Core] (https://foundryvtt.com/packages/ptBR-core/).

If you find any errors in the translation or have a suggestion for a better term to use in some part of it, open a new issue.

### Installation
The translation is available in the list of Complementary Modules to install under the name Translation: Brazilian Portuguese [SWADE].

### Installation by Manifest
In the Add-On Modules option click on Install Module and place the following link in the Manifest URL field

`https://gitlab.com/foundryvtt-pt-br/swade/-/raw/master/swade_pt-BR/module.json`

### Manual Installation
If the above options don't work, download the [swade-master-swade_pt-BR.zip] file (https://gitlab.com/foundryvtt-pt-br/swade/-/archive/master/swade-master. zip? path = swade_pt-BR) and extract the swade_pt-BR folder inside the Data / modules / folder
Once this is done, activate the module in the settings of the world where you intend to use it and then change the language in the settings.